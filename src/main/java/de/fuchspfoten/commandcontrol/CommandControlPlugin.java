package de.fuchspfoten.commandcontrol;

import de.fuchspfoten.commandcontrol.modules.BookCommandBlockerModule;
import de.fuchspfoten.commandcontrol.modules.CommandFirewallModule;
import de.fuchspfoten.commandcontrol.modules.CtrlModule;
import de.fuchspfoten.commandcontrol.modules.HelpModule;
import lombok.Getter;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Main class for the plugin.
 */
public class CommandControlPlugin extends JavaPlugin implements Listener {

    /**
     * Singleton plugin instance.
     */
    private @Getter static CommandControlPlugin self;

    /**
     * Maps command labels to commands.
     */
    private final Map<String, Command> commandMap = new HashMap<>();

    /**
     * The file in which the command data is stored.
     */
    private File commandFile;

    /**
     * The YML configuration in which command data is stored.
     */
    private YamlConfiguration commandStorage;

    @Override
    public void onDisable() {
        // Save all commands.
        for (final Command cmd : commandMap.values()) {
            commandStorage.set(cmd.getLabel() + ".loggable", cmd.isLoggable());
            commandStorage.set(cmd.getLabel() + ".hashelp", cmd.isHelpEntryVisible());
            commandStorage.set(cmd.getLabel() + ".permLevel", cmd.getPermissionLevel());
            commandStorage.set(cmd.getLabel() + ".help", cmd.getHelpMessage());
        }

        // Save the command configuration.
        try {
            commandStorage.save(commandFile);
        } catch (final IOException e) {
            getLogger().severe("Could not save commands.yml");
            e.printStackTrace();
        }
    }

    @Override
    public void onEnable() {
        self = this;

        // Create the default config.
        getConfig().options().copyDefaults(true);
        saveConfig();

        // Create the message configuration, if needed.
        commandFile = new File(getDataFolder(), "commands.yml");
        if (!commandFile.exists()) {
            try {
                if (!commandFile.createNewFile()) {
                    throw new IllegalStateException("creating commands.yml failed: failure");
                }
            } catch (final IOException e) {
                throw new IllegalStateException("creating commands.yml failed", e);
            }
        }
        commandStorage = YamlConfiguration.loadConfiguration(commandFile);

        // Get known commands.
        for (final String label : commandStorage.getKeys(false)) {
            final Command cmd = new Command(label, commandStorage.getBoolean(label + ".loggable"),
                    commandStorage.getBoolean(label + ".hashelp", true),
                    commandStorage.getString(label + ".permLevel"),
                    commandStorage.getString(label + ".help"));
            commandMap.put(label, cmd);
        }

        // Register command modules.
        getCommand("ctrl").setExecutor(new CtrlModule());
        getCommand("help").setExecutor(new HelpModule());

        // Register other modules.
        getServer().getPluginManager().registerEvents(new BookCommandBlockerModule(), this);
        getServer().getPluginManager().registerEvents(new CommandFirewallModule(), this);
    }

    /**
     * Returns the number of commands stored.
     *
     * @return The number of commands stored.
     */
    public int getNumCommands() {
        return commandMap.size();
    }

    /**
     * Fetches the existing commands in sorted order.
     *
     * @return The commands.
     */
    public Collection<Command> getSortedCommands() {
        return new TreeMap<>(commandMap).values();
    }

    /**
     * Registers the given command.
     *
     * @param cmd The command.
     */
    public void registerCommand(final Command cmd) {
        commandMap.put(cmd.getLabel(), cmd);
    }

    /**
     * Unregisters the given command.
     * @param label The command.
     */
    public void unregisterCommand(final String label) {
        commandMap.remove(label);
        commandStorage.set(label, null);
    }

    /**
     * Fetches the command associated with the given label.
     *
     * @param label The label.
     * @return The command.
     */
    public Command getCommandData(final String label) {
        return commandMap.get(label);
    }
}
