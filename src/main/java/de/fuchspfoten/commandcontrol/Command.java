package de.fuchspfoten.commandcontrol;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Represents a command.
 */
@Getter
@Setter
@AllArgsConstructor
public class Command {

    /**
     * The label of the command.
     */
    private String label;

    /**
     * Whether the command is loggable.
     */
    private boolean loggable;

    /**
     * Whether the command has a help entry.
     */
    private boolean helpEntryVisible;

    /**
     * The permission level for the command.
     */
    private String permissionLevel;

    /**
     * The help message for the command.
     */
    private String helpMessage;
}
