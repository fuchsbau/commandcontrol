package de.fuchspfoten.commandcontrol.modules;

import de.fuchspfoten.commandcontrol.Command;
import de.fuchspfoten.commandcontrol.CommandControlPlugin;
import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.Pagination;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.List;
import java.util.stream.Collectors;

/**
 * /help [page] is used to look at command help.
 */
public class HelpModule implements CommandExecutor {

    /**
     * The parser for command arguments for /help.
     */
    private final ArgumentParser argumentParserHelp;

    /**
     * Constructor.
     */
    public HelpModule() {
        Messenger.register("ctrl.helpLine");

        argumentParserHelp = new ArgumentParser();
        argumentParserHelp.addArgument('e', "Set the help string for the given command",
                "command");
        argumentParserHelp.addArgument('v', "Make the command (in-)visible in /help",
                "command");
    }

    @Override
    public boolean onCommand(final CommandSender sender, final org.bukkit.command.Command command, final String label,
                             String[] args) {
        // Syntax check.
        args = argumentParserHelp.parse(args);
        if (argumentParserHelp.hasArgument('h')) {
            Messenger.send(sender, "commandHelp", "Syntax: /help [page]");
            argumentParserHelp.showHelp(sender);
            return true;
        }

        if (argumentParserHelp.hasArgument('v')) {
            // Editing, Permission check.
            if (!sender.hasPermission("ctrl.manage")) {
                sender.sendMessage("Missing permission: ctrl.manage");
                return true;
            }

            final String lbl = argumentParserHelp.getArgument('v');
            final Command cmd = CommandControlPlugin.getSelf().getCommandData(lbl);
            if (cmd == null) {
                sender.sendMessage("Invalid command label.");
                return true;
            }

            final Command overwrite = new Command(cmd.getLabel(), cmd.isLoggable(), !cmd.isHelpEntryVisible(),
                    cmd.getPermissionLevel(), cmd.getHelpMessage());
            CommandControlPlugin.getSelf().registerCommand(overwrite);

            sender.sendMessage("Successfully changed. Command visible in /help: " + overwrite.isHelpEntryVisible());
        } else if (argumentParserHelp.hasArgument('e')) {
            // Editing, Permission check.
            if (!sender.hasPermission("ctrl.manage")) {
                sender.sendMessage("Missing permission: ctrl.manage");
                return true;
            }

            if (args.length < 1) {
                sender.sendMessage("Specify a label.");
                return true;
            }

            final String lbl = argumentParserHelp.getArgument('e');
            final Command cmd = CommandControlPlugin.getSelf().getCommandData(lbl);
            if (cmd == null) {
                sender.sendMessage("Invalid command label.");
                return true;
            }

            final String newHelp = String.join(" ", args);
            final Command overwrite = new Command(cmd.getLabel(), cmd.isLoggable(), cmd.isHelpEntryVisible(),
                    cmd.getPermissionLevel(), newHelp);
            CommandControlPlugin.getSelf().registerCommand(overwrite);

            sender.sendMessage("Successfully changed. New line is");
            Messenger.send(sender, "ctrl.helpLine", '/' + overwrite.getLabel(), overwrite.getHelpMessage());
        } else {
            // Displaying help.
            final int page;
            if (args.length > 0) {
                try {
                    page = Integer.parseInt(args[0]);
                    if (page < 1) {
                        sender.sendMessage("Invalid page number");
                        return true;
                    }
                } catch (final NumberFormatException ex) {
                    sender.sendMessage("Invalid page number");
                    return true;
                }
            } else {
                page = 1;
            }

            // Sort the commands.
            final List<Command> sorted = CommandControlPlugin.getSelf().getSortedCommands().stream()
                    .filter(x -> sender.hasPermission("ctrl.cmd." + x.getPermissionLevel()))
                    .filter(Command::isHelpEntryVisible)
                    .collect(Collectors.toList());
            final int numCommands = sorted.size();

            // Determine pagination metrics.
            int step = 18;
            final int pages = (numCommands % step == 0) ? numCommands / step : numCommands / step + 1;
            int skip = (page - 1) * step;

            // Print the help.
            Pagination.showPagination(sender, page, pages, "/help ");
            for (final Command cmd : sorted) {
                // Page selection.
                if (skip-- > 0) {
                    continue;
                }
                if (step-- == 0) {
                    break;
                }

                // Show the help line.
                Messenger.send(sender, "ctrl.helpLine", '/' + cmd.getLabel(), cmd.getHelpMessage());
            }
            Pagination.showPagination(sender, page, pages, "/help ");
        }

        return true;
    }
}
