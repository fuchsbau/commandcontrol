package de.fuchspfoten.commandcontrol.modules;

import de.fuchspfoten.commandcontrol.Command;
import de.fuchspfoten.commandcontrol.CommandControlPlugin;
import de.fuchspfoten.fuchslib.Messenger;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.TabCompleteEvent;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Command firewall that blocks disallowed commands.
 */
public class CommandFirewallModule implements Listener {

    /**
     * Constructor.
     */
    public CommandFirewallModule() {
        Messenger.register("ctrl.unknown");
        Messenger.register("ctrl.opOverride");
        Messenger.register("ctrl.forbidden");
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerCommandPreprocess(final PlayerCommandPreprocessEvent event) {
        // Override command handling.
        if (event.isCancelled()) {
            return;  // Command is to be ignored.
        }
        event.setCancelled(true);

        final String noSlash = event.getMessage().substring(1);
        final String[] parts = noSlash.split(" ");

        final Command cmd = CommandControlPlugin.getSelf().getCommandData(parts[0]);
        if (cmd == null) {
            // Log command error.
            CommandControlPlugin.getSelf().getLogger().info(event.getPlayer().getName()
                    + " issued invalid command: " + parts[0]);
            Messenger.send(event.getPlayer(), "ctrl.unknown", parts[0]);

            if (event.getPlayer().isOp()) {
                Messenger.send(event.getPlayer(), "ctrl.opOverride");
                event.getPlayer().performCommand(noSlash);
            } else if (event.getPlayer().hasPermission("ctrl.diagnosis")) {
                Messenger.send(event.getPlayer(), "ctrl.forbidden");
            }
        } else {
            // Log, if allowed.
            if (cmd.isLoggable()) {
                CommandControlPlugin.getSelf().getLogger().info(event.getPlayer().getName()
                        + " issued server command: " + event.getMessage());
            }

            // Permission check.
            if (!event.getPlayer().hasPermission("ctrl.cmd." + cmd.getPermissionLevel())) {
                if (event.getPlayer().hasPermission("ctrl.diagnosis")) {
                    Messenger.send(event.getPlayer(), "ctrl.forbidden");
                } else {
                    Messenger.send(event.getPlayer(), "ctrl.unknown", parts[0]);
                }
                return;
            }

            // Issue command.
            event.getPlayer().performCommand(noSlash);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onTabComplete(final TabCompleteEvent event) {
        if (event.isCancelled() || !(event.getSender() instanceof Player)) {
            return;
        }
        final Player player = (Player) event.getSender();

        // Filter out chat messages.
        if (!event.getBuffer().startsWith("/")) {
            return;
        }

        // Default: Clear all completions.
        event.setCompletions(Collections.emptyList());
        final String noSlash = event.getBuffer().substring(1);

        if (event.getBuffer().contains(" ")) {
            // Argument completion.
            final String[] tokens = noSlash.split(" ");
            final String lastToken = noSlash.endsWith(" ") ? "" : tokens[tokens.length - 1].toLowerCase();
            final List<String> completions = Bukkit.getOnlinePlayers().stream()
                    .filter(player::canSee)
                    .filter(x -> x.getName().toLowerCase().startsWith(lastToken))
                    .map(Player::getName)
                    .collect(Collectors.toList());
            event.setCompletions(completions);
        } else {
            // Command completion.
            final boolean allowDoubleSlash = event.getBuffer().startsWith("//");
            final Collection<Command> commands = CommandControlPlugin.getSelf().getSortedCommands();
            final List<String> completions = commands.stream()
                    .filter(x -> x.getLabel().startsWith(noSlash))
                    .filter(x -> event.getSender().hasPermission("ctrl.cmd." + x.getPermissionLevel()))
                    .map(Command::getLabel)
                    .filter(x -> allowDoubleSlash || !x.startsWith("/"))
                    .map(x -> '/' + x)
                    .collect(Collectors.toList());
            event.setCompletions(completions);
        }
    }
}
