package de.fuchspfoten.commandcontrol.modules;

import de.fuchspfoten.fuchslib.Messenger;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Blocks command execution while holding books.
 */
public class BookCommandBlockerModule implements Listener {

    /**
     * Constructor.
     */
    public BookCommandBlockerModule() {
        Messenger.register("ctrl.bookCommandBlocked");
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerCommandPreprocess(final PlayerCommandPreprocessEvent event) {
        final ItemStack hand = event.getPlayer().getInventory().getItemInMainHand();
        final ItemStack offHand = event.getPlayer().getInventory().getItemInOffHand();
        if ((hand != null && hand.getType() == Material.WRITTEN_BOOK)
                || (offHand != null && offHand.getType() == Material.WRITTEN_BOOK)) {
            event.setCancelled(true);
            Messenger.send(event.getPlayer(), "ctrl.bookCommandBlocked");
        }
    }
}
