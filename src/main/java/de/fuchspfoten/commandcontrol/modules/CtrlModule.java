package de.fuchspfoten.commandcontrol.modules;

import de.fuchspfoten.commandcontrol.Command;
import de.fuchspfoten.commandcontrol.CommandControlPlugin;
import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * /ctrl &lt;command&gt; is used to manage commands.
 */
public class CtrlModule implements CommandExecutor {

    /**
     * The parser for command arguments for /ctrl.
     */
    private final ArgumentParser argumentParserCtrl;

    /**
     * Constructor.
     */
    public CtrlModule() {
        argumentParserCtrl = new ArgumentParser();
        argumentParserCtrl.addSwitch('r', "Remove the given command from command storage");
        argumentParserCtrl.addSwitch('x', "Make the given command not loggable");
        argumentParserCtrl.addArgument('p', "Set the permission level for the given command",
                "level");
        argumentParserCtrl.addArgument('l', "List commands", "page");
    }

    @Override
    public boolean onCommand(final CommandSender sender, final org.bukkit.command.Command command, final String label,
                             String[] args) {
        // Permission check.
        if (!sender.hasPermission("ctrl.manage")) {
            sender.sendMessage("Missing permission: ctrl.manage");
            return true;
        }

        // Syntax check.
        args = argumentParserCtrl.parse(args);
        final boolean listing = argumentParserCtrl.hasArgument('l');
        if ((args.length != 1 && !listing) || argumentParserCtrl.hasArgument('h')) {
            Messenger.send(sender, "commandHelp", "Syntax: /ctrl <command>");
            argumentParserCtrl.showHelp(sender);
            return true;
        }

        if (listing) {
            // Determine the page number.
            final int page;
            try {
                page = Integer.parseInt(argumentParserCtrl.getArgument('l'));
                if (page < 1) {
                    sender.sendMessage("Invalid page number");
                    return true;
                }
            } catch (final NumberFormatException ex) {
                sender.sendMessage("Invalid page number");
                return true;
            }

            // Determine pagination metrics.
            final int numCommands = CommandControlPlugin.getSelf().getNumCommands();
            int step = 18;
            final int pages = (numCommands % step == 0) ? numCommands / step : numCommands / step + 1;
            int skip = (page - 1) * step;

            // Sort the commands.
            final Iterable<Command> sorted = CommandControlPlugin.getSelf().getSortedCommands();

            // Print the listing.
            sender.sendMessage(String.format("[Showing page %1$d of %2$d]", page, pages));
            for (final Command cmd : sorted) {
                if (skip-- > 0) {
                    continue;
                }
                if (step-- == 0) {
                    break;
                }
                sender.sendMessage(String.format("/%1$s -- permission: %2$s / loggable: %3$s / help entry: %4$s",
                        cmd.getLabel(), cmd.getPermissionLevel(), Boolean.toString(cmd.isLoggable()),
                        Boolean.toString(cmd.isHelpEntryVisible())));
            }
            return true;
        }

        if (argumentParserCtrl.hasArgument('r')) {
            // Remove the command.
            CommandControlPlugin.getSelf().unregisterCommand(args[0]);
        } else {
            // Determine the help message.
            final Command oldCommand = CommandControlPlugin.getSelf().getCommandData(args[0]);
            final String helpMessage = (oldCommand == null) ? "..." : oldCommand.getHelpMessage();

            // Create the command.
            final String permLevel =
                    argumentParserCtrl.hasArgument('p') ? argumentParserCtrl.getArgument('p') : "op";
            final Command cmd = new Command(args[0], !argumentParserCtrl.hasArgument('x'), true,
                    permLevel, helpMessage);
            CommandControlPlugin.getSelf().registerCommand(cmd);
        }
        sender.sendMessage("modified command " + args[0]);

        return true;
    }
}
